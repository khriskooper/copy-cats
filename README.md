# Welcome to Copy Cats

A simple, yet challenging, memory game featuring increasingly longer sequences of robotic cat sounds for the player to remember and speak back.

Developed as an Alexa Skill using Python 3.6 in AWS Lambda.

The code has been hacked together to get into submission process as quickly as possible.

## Privacy Policy

Copy Cats does not collect any data and so there is nothing to worry about here.

## Terms of Use

Assumes no responsibility for errors or omissions in the contents on the Service.

In no event shall be liable for any special, direct, indirect, consequential, or incidental damages or any damages whatsoever, whether in an action of contract, negligence or other tort, arising out of or in connection with the use of the Service or the contents of the Service. reserves the right to make additions, deletions, or modification to the contents on the Service at any time without prior notice.

## Release info

Passed certification and released on 22 May 2018. Available in the following countries:

Country | URL
--- | ---
USA | https://www.amazon.com/dp/B07D4KFVSP
UK | https://www.amazon.co.uk/dp/B07D4KFVSP
Canada | https://www.amazon.ca/dp/B07D4KFVSP
Australia | https://www.amazon.com.au/dp/B07D4KFVSP

Use the appropriate link and click 'Enable' to try it out on your Alexa device.

## Contact

For any feedback, complaints or suggestions, please email: khriskooper@hotmail.com