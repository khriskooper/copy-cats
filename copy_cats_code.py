from random import choice

max_size = 15 # can't be higher than 24 due to Alexa slot limitations
start_size = 3
start_lives = 9

wrong_answer_choices = ['Not right, you paw thing.','Wrong answer.','Hard luck.','Not quite.','Incorrect.','Incorrect.',"That's wrong."]
correct_answer_choices = ['Meowsome!','Fursome!','Correct!','Well done.','Excellent.','Right!','Perfect!',"That's right.","Furry good."]
doing_badly_choices = ['Try asking me for help.','Can you speak more clearly?', "I'm a little deaf you know!", "I think I'm having trouble understanding you!", "Maybe you should take a cat nap and play again later?"] # used when getting wrong with just 1 size
repeat_threshold_choices = ["Here it is again...","Not sure I should be doing this, but OK...","Is this cheating or not? Anyway...","Hmm, not sure if I should do this but here you go... ","You sure need a lot of help!","Let's try that again..."]

def lambda_handler(event, context):
    
    
    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']}, event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])


def on_session_started(session_started_request, session):
    """ Called when the session starts """

    #print("on_session_started requestId=" + session_started_request['requestId'] + ", sessionId=" + session['sessionId'])

def on_session_ended(session_ended_request, session):
    """ Called when the session ends """

    #print("on_session_ended requestId=" + session_ended_request['requestId'] + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they want """

    #print("on_launch requestId=" + launch_request['requestId'] + ", sessionId=" + session['sessionId'])

    global start_size
    attributes = {}
    attributes['question'] = generate_new_question(session, start_size)
    attributes['highest_size'] = start_size
    attributes['streak'] = 0
    attributes['play_again'] = 'false'
    attributes['quitting'] = 'false'
    global start_lives
    attributes['lives'] = start_lives
    spoken_question = attributes['question']['spoken_question']
    
    return {
                'version': '1.0',
                'sessionAttributes': attributes,
                'response': {
                    'outputSpeech': {
                            'type': 'SSML',
                            'ssml': '<speak>Welcome to Copy Cats! Ask me for help, or repeat after me... ' + spoken_question + '</speak>'
                    },
                    'reprompt': {
                        'outputSpeech': {
                            'type': 'SSML',
                            'ssml': '<speak>Ask me for help, or repeat after me... ' + spoken_question + '</speak>'
                        }
                    },
                    'shouldEndSession': 'false'
                }
            }


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] + ", sessionId=" + session['sessionId'])

    if (session['application']['applicationId'] != "amzn1.ask.skill.9d99ed92-ba9d-480e-b37a-7b9991b4a343"):
        raise ValueError("Invalid Application ID")

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']
    attributes = session['attributes']

    # Dispatch to your skill's intent handlers
    if 'CopyCatsAttempt' in intent_name:
        return copy_cats_attempt(intent, session)
    elif intent_name == "AMAZON.YesIntent":
        if attributes['play_again'] == 'true':
            attributes['play_again'] = 'false'
            return on_launch('', session)
        elif attributes['quitting'] == 'true':
            return handle_session_end_request()
    elif intent_name == "AMAZON.NoIntent":
        if attributes['play_again'] == 'true':
            return handle_session_end_request()
        elif attributes['quitting'] == 'true':
            return repeat_question(session)
    elif intent_name == "AMAZON.RepeatIntent":
        repeats = attributes['question']['repeats']
        repeats += 1
        session['attributes']['question']['repeats'] = repeats
        return repeat_question(session)
    elif intent_name == "AMAZON.HelpIntent":
        return get_help_response(session)
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        attributes['quitting'] = 'true'
        return {
                'version': '1.0',
                'sessionAttributes': attributes,
                'response': {
                    'outputSpeech': {
                            'type': 'SSML',
                            'ssml': "<speak>Are you sure you want to quit?</speak>"
                    },
                    'shouldEndSession': 'false'
                }
            }
    elif intent_name == "AMAZON.FallbackIntent":
        return {
                'version': '1.0',
                'sessionAttributes': attributes,
                'response': {
                    'outputSpeech': {
                            'type': 'SSML',
                            'ssml': "<speak>I'm sorry, I didn't quite catch that. Please try again.</speak>"
                    },
                    'shouldEndSession': 'false'
                }
            }
    else:
        raise ValueError("Invalid intent")

def repeat_question(session):
    attributes = session['attributes']
    attributes['quitting'] = 'false' #gotcha

    if attributes['question']['repeats'] >= 3:
        repeat_dialogue = choice(repeat_threshold_choices)
    else:
        repeat_dialogue = "OK, let me repeat the question for you... "
    repeat_dialogue = repeat_dialogue + ' ' + attributes['question']['spoken_question']
    return {
            'version': '1.0',
            'sessionAttributes': attributes,
            'response': {
                'outputSpeech': {
                        'type': 'SSML',
                        'ssml': "<speak>" + repeat_dialogue + "</speak>"
                },
                'shouldEndSession': 'false'
            }
        }


def get_help_response(session):
    attributes = session['attributes']

    help_dialogue = """Hello! I'm Fliss, the robotic cat. I'm trying to teach you how to speak 'cattish' properly. I want you to repeat a sequence of robotic cat sounds, which get progressively more difficult as you get them right.
You have 9 lives, and a lot to learn.
Each time you get a correct answer, the sequence gets longer. If you get 15 right, you win the game. Perfect!
When you get a wrong answer, you lose a life and are asked a simpler question. Lose all of your 9 lives and you lose the game. Catastrophic!
If at any point you need to hear the sequence again, just ask me to repeat it!
Good luck... you'll need it!"""

    help_dialogue = help_dialogue + ' ' + "Now. Let me repeat the sequence. Try to copy it as clearly as possible... "
    help_dialogue = help_dialogue + attributes['question']['spoken_question']

    return {
                'version': '1.0',
                'sessionAttributes': attributes,
                'response': {
                    'outputSpeech': {
                            'type': 'SSML',
                            'ssml': '<speak>' + help_dialogue + '</speak>'
                    },
                    'shouldEndSession': 'false'
                }
            }

def handle_session_end_request():
    return  {
                'version': '1.0',
                'response': {
                    'outputSpeech': {
                            'type': 'SSML',
                            'ssml': '<speak>Thank you for playing. Goodbye!</speak>'
                    },
                    'shouldEndSession': 'true'
                }
            }

def copy_cats_attempt(intent, session):
    
    correct = 'true'
    
    for slot in intent['slots'].items():
        if slot[1]['resolutions']['resolutionsPerAuthority'][0]['status']['code'] != 'ER_SUCCESS_MATCH':
            correct = 'false'
    
        #Need to check order, match up with session order based on A-Z attempt position
        try:
            if session['attributes']['question'][slot[1]['name']] != slot[1]['resolutions']['resolutionsPerAuthority'][0]['values'][0]['value']['name']:
                correct = 'false'
        except: #should really check for null, not 'any error' here
            correct = 'false'
    
    size = session['attributes']['question']['size']
    if size != len(intent['slots']):
        correct = 'false'

    if correct == 'true':
        return correct_answer(intent, session)
    else:
        return wrong_answer(intent, session)


def correct_answer(intent, session):

    attributes = session['attributes']
    streak = attributes['streak'] + 1
    attributes['streak'] = streak
    lives = attributes['lives']
    highest_size = attributes['highest_size']
    size = session['attributes']['question']['size']
    size += 1

    if highest_size < size:
        attributes['highest_size'] = size

    global max_size
    if size > max_size:
        size = max_size

        winning_dialogue = '<audio src="https://s3.amazonaws.com/ask-soundlibrary/musical/amzn_sfx_trumpet_bugle_03.mp3"/> '
        winning_dialogue = winning_dialogue + 'Congratulations! You have won the game!'
        winning_dialogue = winning_dialogue + ' ' + 'And your highest streak was ' + str(streak) + '. Well done!'
        winning_dialogue = winning_dialogue + '<audio src="https://s3.amazonaws.com/ask-soundlibrary/musical/amzn_sfx_trumpet_bugle_02.mp3"/>'

        winning_dialogue = winning_dialogue + ' ' + "Would you like to play again?"
        attributes['play_again'] = 'true'

        return {
            'version': '1.0',
            'sessionAttributes': attributes,
            'response': {
                'outputSpeech': {
                        'type': 'SSML',
                        'ssml': '<speak>' + winning_dialogue + '</speak>'
                },
                'reprompt': {
                    'outputSpeech': {
                        'type': 'SSML',
                        'ssml': '<speak>Would you like to play again?</speak>'
                    }
                },
                'shouldEndSession': 'false'
            }
        }
    
    if streak == 5:
        dialogue = "You're on a roll now."
    elif streak == 7:
        dialogue = "Making purr-gress. Keep it going!"
    elif streak == 9:
        dialogue = "You're unstoppable!"
    else:
        global correct_answer_choices
        dialogue = choice(correct_answer_choices)
    
    dialogue = dialogue + ' ' + '<audio src="https://s3.amazonaws.com/ask-soundlibrary/foley/amzn_sfx_glasses_clink_01.mp3"/>'

    if size == 5 and lives == start_lives:
        dialogue = dialogue + ' ' + "You must have played this before."
    elif size == (max_size - 1):
        dialogue = dialogue + ' ' + "Only two more levels to go!"
    elif size == max_size:
        dialogue = dialogue + ' ' + "This is the last one, can you do it?"
    else:
        if size == 4 and lives != start_lives:
            dialogue = dialogue + ' ' + "You're getting the hang of this."
        elif size == 6:
            dialogue = dialogue + ' ' + "This is going well for you."
        elif size == 8:
            dialogue = dialogue + ' ' + "Getting a bit tricky, huh?"
        elif size == 10:
            dialogue = dialogue + ' ' + "Even I'm struggling to keep track of this!"
        elif size == 12:
            dialogue = dialogue + ' ' + "Don't mess this up."
        elif size == 14:
            dialogue = dialogue + ' ' + "This is incredible!"

    attributes['question'] = generate_new_question(session, size)
    spoken_question = attributes['question']['spoken_question']
    dialogue = dialogue + ' ' + spoken_question

    return {
        'version': '1.0',
        'sessionAttributes': attributes,
        'response': {
            'outputSpeech': {
                    'type': 'SSML',
                    'ssml': '<speak>' + dialogue + '</speak>'
            },
            'shouldEndSession': 'false'
        }
    }


def wrong_answer(intent, session):
    attributes = session['attributes']
    size = attributes['question']['size']
    size -= 1
    if size == 0:
        size = 1
    previous_streak = attributes['streak']
    attributes['streak'] = 0
    highest_size = attributes['highest_size']

    lives = attributes['lives']
    lives -= 1
    attributes['lives'] = lives

    if lives == 0:
        losing_dialogue = '<audio src="https://s3.amazonaws.com/ask-soundlibrary/musical/amzn_sfx_buzzer_loud_alarm_01.mp3"/>'
        
        if highest_size > 3:
            losing_dialogue = losing_dialogue + ' ' + 'Oh dear! You lost all your lives. Well done for copying ' + str(highest_size) + ' cat sounds in a row!'
        else:
            losing_dialogue = losing_dialogue + ' ' + 'Hard luck, you lost all your lives. Maybe you should ask me for some help next time.'

        losing_dialogue = losing_dialogue + ' ' + "Would you like to play again?"
        attributes['play_again'] = 'true'
        
        return {
            'version': '1.0',
            'sessionAttributes': attributes,
            'response': {
                'outputSpeech': {
                        'type': 'SSML',
                        'ssml': '<speak>' + losing_dialogue + '</speak>'
                },
                'reprompt': {
                    'outputSpeech': {
                        'type': 'SSML',
                        'ssml': '<speak>Would you like to play again?</speak>'
                    }
                },
                'shouldEndSession': 'false'
            }
        }

    dialogue = ''
    if previous_streak >= 4:
        dialogue = '<audio src="https://s3.amazonaws.com/ask-soundlibrary/musical/amzn_sfx_buzzer_small_01.mp3"/>'
        if previous_streak == 4:
            dialogue = dialogue + ' ' + "That's got to hurt."
        elif previous_streak == 5:
            dialogue = dialogue + ' ' + "Ouch! You were doing so well."
        elif previous_streak == 6:
            dialogue = dialogue + ' ' + "Don't give up!"
        elif previous_streak >= 7:
            dialogue = dialogue + ' ' + "Catastrophic!"
        
        dialogue = dialogue + ' ' + "Repeat after me."
    else:
        global wrong_answer_choices
        dialogue = choice(wrong_answer_choices)
        dialogue = dialogue + ' ' + '<audio src="https://s3.amazonaws.com/ask-soundlibrary/musical/amzn_sfx_buzzer_small_01.mp3"/>'

    global doing_badly_choices
    help = choice(doing_badly_choices)

    if size == 1:
        dialogue = dialogue + ' ' + help

    if lives == 1:
        dialogue = dialogue + ' ' + "Only one life left now, don't mess this up!"
    elif lives == 2:
        dialogue = dialogue + ' ' + "You have 2 lives left."
    elif lives == 3:
        dialogue = dialogue + ' ' + "Only 3 lives remaining."
    elif lives == 5:
        dialogue = dialogue + ' ' + "You're running out of lives!"

    attributes['question'] = generate_new_question(session, size)
    spoken_question = attributes['question']['spoken_question']
    dialogue = dialogue + ' ' + spoken_question

    return {
        'version': '1.0',
        'sessionAttributes': attributes,
        'response': {
            'outputSpeech': {
                    'type': 'SSML',
                    'ssml': '<speak>' + dialogue + '</speak>'
            },
            'shouldEndSession': 'false'
        }
    }


def generate_new_question(session, size):
    question = {}
    spoken_question = ""

    for i in range(size):
        key = 'PossibleCatSounds' + chr(65 + i)
        value = choice(['meow','purr','hiss'])
        question[key] = value

        if value == 'meow':
            spoken_value = "MEOW"
        elif value == 'purr':
            spoken_value = "PER"
        elif value == 'hiss':
            spoken_value = "HISS"
        spoken_question = spoken_question + spoken_value + '.'
        if i < (size - 1):
            spoken_question = spoken_question + " "

    question['size'] = size
    question['spoken_question'] = spoken_question
    question['repeats'] = 0
    return question